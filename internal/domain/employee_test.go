package domain

import (
	"testing"
)

func TestEmployee_GetName(t *testing.T) {
	employee := Employee{Id: "123", Name: "John Doe", CompanyId: "company123"}

	// Call the GetName method and check if it returns the expected name.
	expectedName := "John Doe"
	actualName := employee.GetName()
	if actualName != expectedName {
		t.Errorf("Expected name %s, but got %s", expectedName, actualName)
	}
}

func TestEmployee_GetID(t *testing.T) {
	employee := Employee{Id: "123", Name: "John Doe", CompanyId: "company123"}

	// Call the GetID method and check if it returns the expected ID.
	expectedID := "123"
	actualID := employee.GetID()
	if actualID != expectedID {
		t.Errorf("Expected ID %s, but got %s", expectedID, actualID)
	}
}

func TestEmployee_GetCompanyId(t *testing.T) {
	employee := Employee{Id: "123", Name: "John Doe", CompanyId: "company123"}

	// Call the GetCompanyId method and check if it returns the expected company ID.
	expectedCompanyID := "company123"
	actualCompanyID := employee.GetCompanyId()
	if actualCompanyID != expectedCompanyID {
		t.Errorf("Expected company ID %s, but got %s", expectedCompanyID, actualCompanyID)
	}
}

func TestEmployee_SetID(t *testing.T) {
	employee := Employee{Id: "123", Name: "John Doe", CompanyId: "company123"}

	// Call the SetID method to change the ID.
	newID := "456"
	employee.SetID(newID)

	// Check if the ID has been updated.
	if employee.Id != newID {
		t.Errorf("Expected ID %s, but got %s", newID, employee.Id)
	}
}
