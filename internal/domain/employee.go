package domain

type Employee struct {
	Id        string `json:"id"`
	Name      string `json:"name"`
	CompanyId int    `json:"company_id"`
}

func (e *Employee) GetName() string {
	return e.Name
}

func (e *Employee) GetID() string {
	return e.Id
}

func (e *Employee) GetCompanyId() int {
	return e.CompanyId
}

func (e *Employee) SetID(id string) {
	e.Id = id
	return
}
