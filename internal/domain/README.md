# Modelling

Let's show off some SOLID principles here:

### Single Responsibility Principle (SRP):

Each type (Customer and Employee) has a single responsibility: representing a user with specific attributes. They don't have extraneous responsibilities.

### Open/Closed Principle (OCP):

The code is open for extension because you can easily add new types that implement the User interface without modifying existing code. It's closed for modification because you don't need to change the PrintUserInfo function to accommodate new user types.

### Liskov Substitution Principle (LSP):

The Customer and Employee types can be used interchangeably with the User interface. This adheres to LSP, which states that derived types should be substitutable for their base types.

### Interface Segregation Principle (ISP):

The User interface is minimal and specific to its clients. It defines only the methods that are relevant to the clients, adhering to ISP.

### Dependency Inversion Principle (DIP):

The code depends on abstractions (interfaces) rather than concrete implementations. It doesn't depend on specific Customer or Employee types but relies on the User interface, aligning with DIP.