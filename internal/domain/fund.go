package domain

type Fund struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}
