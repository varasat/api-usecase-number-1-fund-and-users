package domain

type Customer struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

func (c *Customer) GetName() string {
	return c.Name
}

func (c *Customer) GetID() string {
	return c.Id
}

func (c *Customer) GetCompanyId() int {
	return 0
}

func (c *Customer) SetID(id string) {
	c.Id = id
}
