package get

import (
	"gorm.io/gorm"
	"isa-funds-api/internal/handlers/helpers"
	"isa-funds-api/internal/repositories"
	"net/http"
)

type UserGetHandler struct {
	DB *gorm.DB
}

func NewUserGetHandler(db *gorm.DB) *UserGetHandler {
	return &UserGetHandler{DB: db}
}

func (c *UserGetHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Method not allowed"))
		return
	}

	userId := r.URL.Query().Get("user_id")
	if userId == "" {
		w.WriteHeader(http.StatusNotAcceptable)
		w.Write([]byte("User Id is empty"))
		return
	}
	result, err := repositories.GetUserWithInvestments(userId, c.DB)

	if err == gorm.ErrRecordNotFound {
		w.WriteHeader(http.StatusNotFound)
		w.Write([]byte("User not found"))
		return
	}

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something went wrong"))
		return
	}

	helpers.WriteResponse(w, result, http.StatusOK)
	return
}
