package models

import (
	"gorm.io/gorm"
	"isa-funds-api/internal/domain"
)

type FundData struct {
	Id   *uint  `gorm:"primaryKey"`
	Name string `json:"user_id"` // UserID is used as a foreign key
}

type FundResponse struct {
	Funds []FundData `json:"funds"`
}

func (FundData) TableName() string {
	return "funds"
}

func (f FundData) SaveData(db *gorm.DB) error {
	err := db.Save(&f).Error
	if err != nil {
		return err
	}
	return nil
}

func ToFundDbModel(fund domain.Fund) FundData {
	return FundData{
		Name: fund.Name,
	}
}

func GetFundDataFromId(id string, db *gorm.DB) (*FundData, error) {
	var fd FundData
	err := db.First(&fd, id).Error
	if err != nil {
		return nil, err
	}

	return &fd, nil
}
