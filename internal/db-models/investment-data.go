package models

import (
	"gorm.io/gorm"
	"isa-funds-api/internal/domain"
)

// InvestmentData : UserId and FundId are used as a foreign keys
type InvestmentData struct {
	UserId string  `json:"user_id"`
	FundId int     `json:"fund_id"`
	Amount float64 `json:"amount"`
}

func (InvestmentData) TableName() string {
	return "investments"
}

func (i InvestmentData) SaveData(db *gorm.DB) error {
	err := db.Create(&i).Error
	if err != nil {
		return err
	}
	return nil
}

func ToInvestmentDbModel(investment domain.Investment) InvestmentData {
	return InvestmentData{
		UserId: investment.UserId,
		FundId: investment.FundId,
		Amount: investment.Amount,
	}
}
