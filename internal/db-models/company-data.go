package models

import (
	"gorm.io/gorm"
	"isa-funds-api/internal/domain"
)

type CompanyData struct {
	Id   *int   `gorm:"primaryKey" ,json:"id"`
	Name string `json:"name"` // UserID is used as a foreign key
}

type CompanyResponse struct {
	Companies []CompanyData `json:"companies"`
}

func (CompanyData) TableName() string {
	return "companies"
}

func (c CompanyData) SaveData(db *gorm.DB) error {
	err := db.Create(&c).Error
	if err != nil {
		return err
	}
	return nil
}

func ToCompanyDbModel(company domain.Company) CompanyData {
	return CompanyData{
		Name: company.Name,
	}
}

func GetCompanyDataFromId(id string, db *gorm.DB) (*CompanyData, error) {
	var cd CompanyData
	err := db.First(&cd, id).Error
	if err != nil {
		return nil, err
	}

	return &cd, nil
}
