package repositories

import (
	"gorm.io/gorm"
	models "isa-funds-api/internal/db-models"
	domainInterface "isa-funds-api/internal/domain/interfaces"
)

type User struct {
	Id          string `gorm:"primaryKey"`
	Name        string
	CompanyId   int
	Investments []Investment
}

type Investment struct {
	UserId string `gorm:"foreignKey:UserId"`
	Amount float64
	FundId int `gorm:"foreignKey:FundId"`
	Fund   Fund
}

type Fund struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type UserWithInvestments struct {
	ID          string               `json:"id"`
	Name        string               `json:"name"`
	Investments []InvestmentResponse `json:"investments"`
}

type InvestmentResponse struct {
	Amount float64 `json:"amount"`
	Fund   Fund    `json:"fund"`
}

func GetUserWithInvestments(userID string, db *gorm.DB) (*UserWithInvestments, error) {
	user, err := GetUserWithInvestmentsData(userID, db)
	if err != nil {
		return nil, err
	}
	userWithInvestments := UserWithInvestments{
		ID:   user.Id,
		Name: user.Name,
	}
	for _, investment := range user.Investments {
		investmentResponse := InvestmentResponse{
			Amount: investment.Amount,
			Fund: Fund{
				ID:   investment.Fund.ID,
				Name: investment.Fund.Name,
			},
		}
		userWithInvestments.Investments = append(userWithInvestments.Investments, investmentResponse)
	}
	return &userWithInvestments, err
}

func GetUserWithInvestmentsData(userID string, db *gorm.DB) (*User, error) {
	var user User
	err := db.Preload("Investments").First(&user, "id = ?", userID).Error
	if err != nil {
		return nil, err
	}

	for i := range user.Investments {
		if err = db.Model(&user.Investments[i]).Association("Fund").Find(&user.Investments[i].Fund); err != nil {
			return nil, err
		}
	}
	return &user, nil
}

func UserCanHaveMoreInvestment(investment models.InvestmentData, db *gorm.DB) (bool, error) {
	user, err := GetUserWithInvestmentsData(investment.UserId, db)
	if err != nil {
		return false, err
	}

	if user.CompanyId != 0 {
		return true, nil
	}

	countInvestments := len(user.Investments)
	if countInvestments > 0 {
		return false, nil
	}

	return true, nil
}

func ToUserDbModel(user domainInterface.User) models.UserData {
	userId := user.GetID()
	companyId := user.GetCompanyId()
	if companyId != 0 {
		return models.UserData{
			ID:        &userId,
			Name:      user.GetName(),
			CompanyId: &companyId,
		}
	}
	return models.UserData{
		ID:   &userId,
		Name: user.GetName(),
	}
}
