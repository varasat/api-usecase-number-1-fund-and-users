### Todo list for this project
- [X] model the db tables
- [x] create domain models
- [x] create database models
- [x] create tests for at least a handler
- [x] create handler for adding a user
- [X] create handler for adding a fund and a contribution from a user
- [x] check if user is assigned to a company 
and if they are then do not allow them to use more than 1 different fund

### Todo list for improving this project

- Replace net/http with mux
- Add created_on and modified_on time columns to the tables to be able to track user behaviour
- Add a table linking companies to funds. This would allow employees to view specific funds related to their company
- Play around with design patterns, maybe a decorator pattern would work in this case, would need more domain knowledge
- Add a database migration tool eg: Flyway to do all the imports
- Add an Api key and a middleware to manage it
- Add dlv and a remote listener for debugging
- Add an endpoint to add a company to a user
- Add an endpoint to delete a user/investment/company/fund
- Add tests for everything else
- Add a git ignore