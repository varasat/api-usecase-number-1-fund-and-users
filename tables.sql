-- Homework : You can find tools that do migrations and DB updates for you EG: Flyway
CREATE TABLE companies (id SERIAL PRIMARY KEY, name VARCHAR);
CREATE TABLE users (id VARCHAR PRIMARY KEY, name VARCHAR, company_id INT);
CREATE TABLE funds (id SERIAL PRIMARY KEY, name VARCHAR);
CREATE TABLE investments (
                             user_id VARCHAR REFERENCES users(id),
                             fund_id INTEGER REFERENCES funds(id),
                             amount FLOAT
);

-- This example assumes that company_id references the id column in the companies table.
ALTER TABLE users
    ADD CONSTRAINT fk_company_id
        FOREIGN KEY (company_id)
            REFERENCES companies(id);


