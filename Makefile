# Makefile for building and running the Go API and PostgreSQL Docker containers

.PHONY: build run

build:
	docker-compose build

run: build
	docker-compose up
