package main

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	handlers "isa-funds-api/internal/handlers"
	"isa-funds-api/internal/handlers/get"
	post "isa-funds-api/internal/handlers/post"
	"log"
	"net/http"
)

func main() {
	db, err := gorm.Open(postgres.New(postgres.Config{
		DSN: "host=db user=myuser password=mypassword dbname=customer-db port=5432 sslmode=disable",
	}), &gorm.Config{})
	if err != nil {
		log.Fatal("Failed to connect to database:", err)
	}
	//eventual todo: use mux instead to have a url like user/get/{user_id} instead of user/get?user_id={user_id}
	//would be something like
	//		vars := mux.Vars(r)
	//    	userID := vars["user_id"]

	fmt.Println("connected to db")
	userAddEndpoint := post.NewUserAddHandler(db)
	companyAddEndpoint := post.NewCompanyAddHandler(db)
	fundAddEndpoint := post.NewFundAddHandler(db)
	investmentAddEndpoint := post.NewInvestmentAddHandler(db)
	userGetEndpoint := get.NewUserGetHandler(db)
	companyGetEndpoint := get.NewCompanyGetHandler(db)
	fundGetEndpoint := get.NewFundGetHandler(db)

	http.HandleFunc("/health", handlers.HealthHandler)                 // health endpoint for the api
	http.HandleFunc("/user/add", userAddEndpoint.ServeHTTP)            // Creates a basic user
	http.HandleFunc("/user/get", userGetEndpoint.ServeHTTP)            // Gets all user data
	http.HandleFunc("/company/add", companyAddEndpoint.ServeHTTP)      // Add a company to the system
	http.HandleFunc("/company/get", companyGetEndpoint.ServeHTTP)      // retrieves a company's details from its id
	http.HandleFunc("/fund/add", fundAddEndpoint.ServeHTTP)            // adds a fund to the list of funds
	http.HandleFunc("/fund/get", fundGetEndpoint.ServeHTTP)            //gets all the funds
	http.HandleFunc("/user/add/fund", investmentAddEndpoint.ServeHTTP) //creates a link between a fund and a user todo: finish this

	log.Fatal(http.ListenAndServe(":8080", nil))
}
